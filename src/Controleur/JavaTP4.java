/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controleur;
import Exceptions.*;
import Modele.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
//import Vue.*;
/**
 *
 * @author Nicolas Lelouche
 */
public class JavaTP4 {

    /**
     * @param args the command line arguments
     * @throws Exceptions.FondsExistant
     */
    public static void main(String[] args) throws FondsExistant {
        
        //EXO 1.5
        System.out.println("////Test ajout Fonds(Exercice 1.5)\\\\");
        System.out.println("Entrez le nom d'un fonds: ");
        Scanner new_scanner = new Scanner(System.in);
        String key_fonds1 = new_scanner.nextLine();
        System.out.println("Entrez la valeur de ce fonds: ");
        double valeur_fonds1 = new_scanner.nextDouble();
        Portefeuille wallet1 = new Portefeuille();
        try {
            double dummy_amount = wallet1.getFonds(key_fonds1);
            throw new FondsExistant();
        } catch (FondsInexistant ex) {
            wallet1.addFond(key_fonds1, valeur_fonds1);
        }
        System.out.println("\n");
        
        //EXO 1.6
        System.out.println("////Test ajout Instrument(Exercice 1.6)\\\\");
        System.out.println("Entrez le nom de l'instrument: ");
        String key_instrument1 = new_scanner.nextLine();
        try {
            ArrayList<Fonds> dummy_array = wallet1.getInstrument(key_instrument1);
        } catch (InstrumentInexistant ex) {
            Instrument new_instrument = new Instrument();
            wallet1.addInstrument(key_instrument1, new_instrument);
            new_instrument.addValeursNom(key_fonds1, valeur_fonds1);
        }
        System.out.println("\n");
        
        //EXO 1.7
        System.out.println("////Test supprimer Fonds (Exercice 1.7.1)\\\\");
        System.out.println("Rentrez la clef du fonds a supprimer: ");
        String key_fonds_a_supprimer = new_scanner.nextLine();
        wallet1.supprimerFonds(key_fonds_a_supprimer);
        System.out.println("////Test supprimer Instrument (Exercice 1.7.2)\\\\");
        System.out.println("Rentrez la clef de l'instrument a supprimer: ");
        String key_instrument_a_supprimer = new_scanner.nextLine();
        wallet1.supprimerInstrument(key_instrument_a_supprimer);
        
    }
    
}
