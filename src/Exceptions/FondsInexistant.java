/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exceptions;

/**
 *
 * @author Nicolas Lelouche
 */
public class FondsInexistant extends Exception {

    /**
     * Creates a new instance of <code>FondsInexistant</code> without detail
     * message.
     */
    public FondsInexistant() {
        super("Le fond demande n'existe pas");
    }
}
