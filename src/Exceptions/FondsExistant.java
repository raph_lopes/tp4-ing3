/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exceptions;

/**
 *
 * @author Nicolas Lelouche
 */
public class FondsExistant extends Exception {

    /**
     * Creates a new instance of <code>FondsExistant</code> without detail
     * message.
     */
    public FondsExistant() {
        super("Le fond existe");
    }

    /**
     * Constructs an instance of <code>FondsExistant</code> with the specified
     * detail message.
     *
     * @param msg the detail message.
     */
}
