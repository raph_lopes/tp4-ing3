/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exceptions;

/**
 *
 * @author Nicolas Lelouche
 */
public class InstrumentInexistant extends Exception {

    /**
     * Creates a new instance of <code>InstrumentInexistant</code> without
     * detail message.
     */
    public InstrumentInexistant() {
         super("L'instrument n'existe pas");
    }
}
