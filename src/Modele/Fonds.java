/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modele;

/**
 *
 * @author Nicolas Lelouche
 */

public class Fonds {
    //Attributes of all the funds
    
    //Amount of the fund
    private double amount;
    //Name of the fund
    private String nom;
    
    //Default constructor
    public Fonds(){
        amount = 0;
        nom  = "";
    }
    
    //Overloaded constructor
    public Fonds(double new_amount, String new_nom)
    {
        amount = new_amount;
        nom = new_nom;
    }
    
    Fonds(double new_amount)
    {
        amount = new_amount;
    }
    //Amount getter
    double getAmount()
    {
        return amount;
    }
    
    //Name getter
    String getNom()
    {
        return nom;
    }
    
    //Name setter
    void setNom(String new_nom)
    {
        nom = new_nom;
    }
    
    //Amount setter
    void setAmount(double new_amount)
    {
        amount = new_amount;
    } 
}
