/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modele;

import Exceptions.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nicolas Lelouche
 */
public class Portefeuille {
    HashMap<String,Fonds> hashFonds;
    HashMap<String,Instrument> hashInstrument;
    String nomPortefeuille;
    
    public Portefeuille()
    {
        hashFonds = new HashMap<>();
        hashInstrument = new HashMap<>();
        nomPortefeuille = "<Empty string>";
    }
    
    public Portefeuille(String new_nom)
    {
        nomPortefeuille = new_nom;
        Fonds first_fond = new Fonds();
        Instrument first_instrument = new Instrument();
        hashFonds = new HashMap<>();
        hashInstrument = new HashMap<>();
        hashFonds.put(nomPortefeuille, first_fond);
        hashInstrument.put(nomPortefeuille,first_instrument);
    }
    
    public void setName(String new_nom)
    {
        nomPortefeuille = new_nom;
    }
    
    public void addFond(String key, double valeur)
    {
        Fonds new_fonds = new Fonds(valeur);
        hashFonds.put(key, new_fonds);
    }
    
    public void addInstrument(String key, Instrument valeur)
    {
        hashInstrument.put(key, valeur);
    }
    
    public void addFondtoInstrument(String key, Fonds valeur) throws InstrumentInexistant
    {
        getInstrument(key).add(valeur);
    }
    
    public String getName()
    {
        return nomPortefeuille;
    }
    
    public double getFonds(String key) throws FondsInexistant
    {
        if(hashFonds.containsKey(key))
        {
            return hashFonds.get(key).getAmount();
        }
        else throw new FondsInexistant();
    }
    
    public ArrayList<Fonds> getInstrument(String key) throws InstrumentInexistant
    {
        if(hashInstrument.containsKey(key))
        {
            return hashInstrument.get(key).getValeurs();
        }
        else throw new InstrumentInexistant();
    }
    
    public void supprimerFonds(String key)
    {
        try
        {
          double dummy_amount = getFonds(key);
          hashFonds.remove(key);
        } catch (FondsInexistant ex) {
            System.out.println("Le fond n'existe deja pas!");
        }
    }
    
    public void supprimerInstrument(String key)
    {
        try
        {
          ArrayList<Fonds> dummy_array = getInstrument(key);
          hashInstrument.get(key).getValeurs().clear();
          hashInstrument.remove(key);
        } catch (InstrumentInexistant ex) {
            System.out.println("L'instrument n'existe deja pas!");
        }
    }
}
