/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modele;

import java.util.ArrayList;

/**
 *
 * @author Nicolas Lelouche
 */
public class Instrument {
    
    //Attributes of the instrument class
    
    //An arraylist of all the funds in the instrument
    private ArrayList<Fonds> valeurs;
    //Name of the instrument
    private String nom;
    
    //Default cnstructor
    public Instrument(){
        valeurs = new ArrayList();
        nom  = "";
    }
    
    //Overloaded cnstructor
    public Instrument(ArrayList<Fonds> new_valeurs, String new_nom)
    {
        valeurs = new_valeurs;
        nom = new_nom;
    }
    
    //Values getter
    public ArrayList<Fonds> getValeurs()
    {
        return valeurs;
    }
    
    //Name getter
    public String getNom()
    {
        return nom;
    }
    //Values setter
    public void setNom(String new_nom)
    {
        nom = new_nom;
    }
    
    //Name setter
    public void setValeurs(ArrayList<Fonds> new_valeurs)
    {
        valeurs = new_valeurs;
    }
    
    public void addValeursNom(String nom, double amount)
    {
        Fonds new_fonds = new Fonds(amount,nom);
        addFonds(new_fonds);
    }
    
    public void addFonds(Fonds new_fonds)
    {
        valeurs.add(new_fonds);
    }
}
